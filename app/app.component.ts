import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'My Todo';
  todos =[ {
  	label : 'bring milk',
  	done:false,
   	priority:3},
   	{
  	label : 'mobile pill',
  	done:true,
   	priority:3}
   	,{
  	label : 'clean house',
  	done:false,
   	priority:3},
   	{
  	label : 'fix bulb',
  	done:false,
   	priority:3},
   	 {
   	 	label : 'watch timmy ternar',
  	done:false,
   	priority:3
   	 }


];

addTodo(newTodoLabel){
	var newTodo= {
		label : newTodoLabel,
  	done:false,
   	priority:3
	};
	this.todos.push(newTodo);
}

deleteTodo(todo){
	this.todos = this.todos.filter(t=>t.label !== todo.label);


}
}
/* todos =[ {
  	label : 'bring milk',
  	done:false,
   	priority:3},
   	{
  	label : 'mobile pill',
  	done:false,
   	priority:3}
   	,{
  	label : 'clean house',
  	done:false,
   	priority:3},
   	{
  	label : 'fix bulb',
  	done:false,
   	priority:3},
}

];
*/
